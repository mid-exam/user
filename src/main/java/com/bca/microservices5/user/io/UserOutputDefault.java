package com.bca.microservices5.user.io;

import javax.persistence.Entity;

import lombok.Data;

@Data
public class UserOutputDefault {
	
	private String message;
	private String response_code;
}
