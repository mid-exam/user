package com.bca.microservices5.user.io;

import lombok.Data;

@Data
public class ChangePasswordInput {
	
	private String id;
	private String oldPassword;
	private String newPassword;
	private String retypePassword;
	
}
