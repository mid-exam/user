package com.bca.microservices5.user.io;

import javax.persistence.Entity;

import lombok.Data;

@Data
public class LoginInput {
	
	private String userid;
	private String password;
}
