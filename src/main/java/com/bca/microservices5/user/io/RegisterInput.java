package com.bca.microservices5.user.io;

import lombok.Data;

@Data
public class RegisterInput {

	private String userid;
	private String password;
	private String name;
	private String email;
}
