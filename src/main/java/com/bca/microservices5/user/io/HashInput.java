package com.bca.microservices5.user.io;

import lombok.Data;

@Data
public class HashInput {
	
	private String plainText;
}
