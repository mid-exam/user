package com.bca.microservices5.user.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.microservices5.user.constant.UserConstant;
import com.bca.microservices5.user.entity.User;
import com.bca.microservices5.user.repo.UserRepo;

@Service
public class UserService {
	
	@Autowired
	private UserRepo userRepo;
	
	public User userLogin(String username,String password) {
		
		String hashPassword = hashPassword(password);
		User user = userRepo.findByUseridAndPasswordAndStatus(username, hashPassword,UserConstant.STATUS_ACTIVE);
		
		if(user!=null) {
			return user;
		}
		else return null;	
	}
	
	public String hashPassword(String plainPassword) {
		String hashedPassword = DigestUtils.sha256Hex(plainPassword);
		
		return hashedPassword;
	}
	
	public User changePassword(String newPass,String oldPass,String userid) {
		User user = userRepo.findByUserid(userid);
		
		String oldPassHash = hashPassword(oldPass);
		
		if(user!=null) {
			if(user.getPassword().equals(oldPassHash)) {
				user.setPassword(hashPassword(newPass));
				userRepo.save(user);
				return user;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}
	
	public User register(String userid,String pass,String nama,String email) {
		User user = new User();
		
		user.setName(nama);
		user.setEmail(email);
		user.setUserid(userid);
		user.setPassword(hashPassword(pass));
		user.setStatus(UserConstant.STATUS_ACTIVE);
		user = userRepo.save(user);
		
		return user;
	}
	
}
