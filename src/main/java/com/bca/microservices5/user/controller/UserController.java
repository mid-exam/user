package com.bca.microservices5.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bca.microservices5.user.entity.User;
import com.bca.microservices5.user.io.ChangePasswordInput;
import com.bca.microservices5.user.io.HashInput;
import com.bca.microservices5.user.io.LoginInput;
import com.bca.microservices5.user.io.RegisterInput;
import com.bca.microservices5.user.io.UserOutputDefault;
import com.bca.microservices5.user.repo.UserRepo;
import com.bca.microservices5.user.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	private UserRepo userRepo;

	@PostMapping("/login")
	public UserOutputDefault login(@RequestBody LoginInput input) {
		UserOutputDefault output = new UserOutputDefault();

		if (input.getUserid().length() <= 15) {
			User user = userService.userLogin(input.getUserid(), input.getPassword());

			if (user != null) {
				output.setResponse_code("00");
				output.setMessage("Login Sukses");
			} else {
				output.setResponse_code("99");
				output.setMessage("Login gagal");
			}
		}else {
			output.setResponse_code("99");
			output.setMessage("User ID tidak sesuai");
		}

		return output;
	}

	@PostMapping("/hash")
	public UserOutputDefault hash(@RequestBody HashInput input) {
		UserOutputDefault output = new UserOutputDefault();

		String hashPassword = userService.hashPassword(input.getPlainText());

		if (hashPassword == null || hashPassword.equals("")) {
			output.setResponse_code("99");
			output.setMessage("Gagal hashing dengan plainText (" + input.getPlainText() + ")");

		} else {
			output.setResponse_code("00");
			output.setMessage("Sukses hashing : " + hashPassword);
		}
		return output;
	}

	@PostMapping("/changePassword")
	public UserOutputDefault changePassword(@RequestBody ChangePasswordInput input) {
		UserOutputDefault output = new UserOutputDefault();

		if(input.getNewPassword().equals(input.getRetypePassword())) {
			
			User user = userService.changePassword(input.getNewPassword(), input.getOldPassword(), input.getId());
			if(user!=null) {
				output.setMessage("Berhasil ganti password");
				output.setResponse_code("00");
			}else {
				output.setResponse_code("99");
				output.setMessage("Gagal ganti password");
			}
		}else {
			output.setResponse_code("99");
			output.setMessage("Gagal ganti password");
		}
		return output;
	}
	
	@PostMapping("/register")
	public UserOutputDefault register(@RequestBody RegisterInput input) {
		UserOutputDefault output = new UserOutputDefault();
		
		User user = userRepo.findByUserid(input.getUserid()); 
		
		if(user==null) {
			User newUser = userService.register(input.getUserid(), input.getPassword(), input.getName(), input.getEmail());
			if(newUser!=null) {
				output.setResponse_code("00");
				output.setMessage("Sukses Registrasi");
			}else {
				output.setResponse_code("99");
				output.setMessage("Gagal Registrasi");
			}
		}else {
			output.setResponse_code("99");
			output.setMessage("User ID pernah terdaftar");
		}
		
		return output;
	}
	
}
