package com.bca.microservices5.user.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.bca.microservices5.user.entity.User;

public interface UserRepo extends PagingAndSortingRepository<User, String>{
	
	User findByUseridAndPasswordAndStatus(String id,String password,String status);
	User findByUserid(String id);
}
