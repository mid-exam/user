package com.bca.microservices5.user.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Table(name = "user")
@Data @Entity
public class User {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "userid")
	private String userid;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "nama")
	private String name;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "status")
	private String status;
	
}
